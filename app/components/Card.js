var React = require('react');

var Card = React.createClass({
  render: function(props) {
    var card = "./cards/"+this.props.src+".png";
    return <img src={card} style={{height:128}}/>;
  }
});

module.exports = Card;
