var React = require('react');
var Card = require('./Card');
var cookie = require ('react-cookie');

var Shuffle = React.createClass( {
  getInitialState: function() {
    return this.state = {playingCards: (cookie.load('three-cards-cookie') ? cookie.load('three-cards-cookie') : ['ah','2h','3h','4h','5h','6h','7h','8h','9h','10h',
        'jh','qh','kh','ad','2d','3d','4d','5d','6d','7d','8d','9d','10d','jd',
        'qd','kd','ac','2c','3c','4c','5c','6c','7c','8c','9c','10c','jc','qc',
        'kc','ad','2d','3d','4d','5d','6d','7d','8d','9d','10d','jd','qd','kd']),

      showComponent: (cookie.load('show-component-cookie') ? cookie.load('show-component-cookie') : false)
    }
    this._onButtonClick = this._onButtonClick.bind(this);
  },

  _onButtonClick: function() {
    this.setState({
      showComponent: true,
    });
    cookie.save('show-component-cookie', true)
},

  //Borrowed shuffle function from: https://github.com/Daplie/knuth-shuffle/
  shuffleCards: function(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  },

  handleClick: function() {
    var shuffled = this.shuffleCards(this.state.playingCards);
    this.setState({playingCards: shuffled});
    this._onButtonClick();
    cookie.save('three-cards-cookie', shuffled)

  },

  render: function() {
    return(
      <div className="layout">
        <div className="cards">
        {this.state.showComponent ? <Card src={this.state.playingCards[1]}/> : null}
        {this.state.showComponent ? <Card src={this.state.playingCards[2]}/> : null}
        {this.state.showComponent ? <Card src={this.state.playingCards[3]}/> : null}
        <br/>
        </div>
        <div>
          <button onClick={this.handleClick}>DEAL</button>
        </div>
      </div>
    );
  }
});

module.exports = Shuffle;
